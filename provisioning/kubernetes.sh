#!/bin/bash

set -o errexit

# URL = gitlab.devcircus.com
SERVER_SUBDOMAIN="gitlab"
SERVER_DOMAIN="devcircus.com"

function installGitlabRunner {
  # Download one of the binaries for your system
  sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
  # Give it permissions to execute
  sudo chmod +x /usr/local/bin/gitlab-runner
  # Create a GitLab CI user
  sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
  # Install and run as service:
  sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
  sudo gitlab-runner start
}

function configureSSL {
	# Import the certificates
	pushd /etc/ssl/private
		cp /vagrant/config/certs/* .
		chmod 400 $SERVER_DOMAIN-keypair.pem
		cp $SERVER_DOMAIN-crt.pem /usr/local/share/ca-certificates/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.crt
		update-ca-certificates --verbose
	popd
	# Link the certs
	install -m 700 -o root -g root -d /etc/gitlab-runner/certs
	ln -s /etc/ssl/private/$SERVER_DOMAIN-crt.pem /etc/gitlab-runner/certs/$SERVER_SUBDOMAIN.$SERVER_DOMAIN.crt
}

echo "Setup Gitlab Runner"

installGitlabRunner
configureSSL

echo "Gitlab Runner setup complete"
